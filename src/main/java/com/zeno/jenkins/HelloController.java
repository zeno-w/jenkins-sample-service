package com.zeno.jenkins;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author zeno.w
 */
@RestController
public class HelloController {

    @RequestMapping
    public String hello(){
        return "hello jenkins";
    }

}
